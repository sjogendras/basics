module.exports = function(grunt) {   
  
    // Project configuration.
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        babel : {
            options: {
                sourceMap: false,
                presets: ["es2015"]
            },
            dist: {
                files: {
                "dist/index.compiled.js": "src/index.js"
                }
            }            
        },
        clean: {
            folder: ['dist']       
        }
    });
  

    // Load the plugin that provides the "babel" task.
    grunt.loadNpmTasks('grunt-babel');

    // To clean up
    grunt.loadNpmTasks('grunt-contrib-clean');

    // Default task(s).
    grunt.registerTask('default', ['clean','babel']);
};