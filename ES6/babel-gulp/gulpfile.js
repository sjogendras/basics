var gulp = require("gulp");
var babel = require("gulp-babel");
var rename = require('gulp-rename');

gulp.task("default", function () {
  return gulp.src("src/index.js")
    .pipe(babel(
        {presets: ["es2015"]}
    ))
    .pipe(rename('index.compiled.js'))
    .pipe(gulp.dest("dist"));
});